use std::future::Future;
use std::time::Duration;

#[cfg(feature = "tokio")]
use tokio::{time};

/// similar to JS setInterval.
/// Taken from https://users.rust-lang.org/t/setinterval-in-rust/41664 with a slight modification to assume duration is in milliseconds
#[cfg(feature = "tokio")]
pub fn set_interval<F, Fut>(mut f: F, time: u64) where F: Send + 'static + FnMut() -> Fut, Fut: Future<Output=()> + Send + 'static {
    let dur = Duration::from_millis(time);
    let mut interval = time::interval(dur);
    tokio::spawn(async move {
        interval.tick().await;
        loop {
            interval.tick().await;
            tokio::spawn(f());
        }
    });
}
