use std::f32::consts::PI;
use std::mem::size_of;
use std::rc::Rc;
use glam::{Mat4, Vec3};
use yoi_math::core::to_radians;

// Note - slowly planning on deprecating this 


/// Wrapper around the basic idea of a camera.
#[derive(Copy, Clone)]
pub struct Camera {
    projection: Mat4,
    view: Mat4,
    eye: Vec3,
    target: Vec3,
    up: Vec3,
}

/// Data object for holding the main matrices of a camera to make it easier to send to a descriptor.
#[derive(Copy, Clone)]
pub struct CameraMatrices {
    pub proj: Mat4,
    pub view: Mat4,
}

impl Camera {
    pub fn new() -> Self {
        let mut cam = Camera {
            projection: Mat4::IDENTITY,
            view: Mat4::IDENTITY,
            eye: Vec3::new(0.0, 0.0, 0.0),
            target: Vec3::new(0.0, 0.0, 0.0),
            up: Vec3::new(0.0, 1.0, 0.0), // y is flipped to maintain correct orientation
        };

        // set a default zoom so we can make sure to see something on the screen.
        cam.set_zoom(-0.4);

        cam
    }

    /// flips the y value in the up vector for the camera.
    /// This is necessary in some cases in Vulkan (but could also be done in-shader)
    pub fn flip_y(&mut self) {
        self.up.y = self.up.y * -1.0;
    }

    /// Creates a right-handed perspective projection matrix
    pub fn create_perspective(&mut self, fov: f32, aspect: f32, near: f32, far: f32) {
        self.projection = Mat4::perspective_rh(to_radians(fov), aspect, near, far);
    }

    /// Creates a projection that places origin at top left similar to the default in Processing.
    pub fn top_left_2d_projection(&mut self, window_width: f32, window_height: f32) {
        self.create_ortho(window_width, 0.0, 0.0, window_height, 400.0, -400.0);
        self.projection.w_axis.x *= -1.0;
        self.set_zoom(1.0);
    }

    /// creates a right-handed orthographic projection matrix
    pub fn create_ortho(&mut self, left: f32, right: f32, bottom: f32, top: f32, near: f32, far: f32) {
        self.projection = Mat4::orthographic_rh(left, right, bottom, top, near, far);
    }

    /// Returns currently calculated projection and view matrices of the camera
    /// Returns a [CameraMatrices] object that contains the projection and view matrices.
    pub fn get_matrices(&self) -> CameraMatrices {
        CameraMatrices {
            proj: self.projection,
            view: self.view,
        }
    }

    /// Returns currently calculated inverse projection and view matrices of the camera
    /// Returns a [CameraMatrices] object that contains the inverse projection and view matrices.
    pub fn get_inverse_matrices(&self) -> CameraMatrices {
        CameraMatrices {
            proj: self.projection.inverse(),
            view: self.view.inverse(),
        }
    }

    /// Resets the projection and view matrices.
    pub fn reset(&mut self) {
        self.projection = Mat4::IDENTITY;
        self.view = Mat4::IDENTITY;
    }

    /// returns the projection matrix
    pub fn get_projection(&self) -> Mat4 {
        self.projection.clone()
    }

    /// returns the view matrix
    pub fn get_view(&self) -> Mat4 {
        self.view.clone()
    }

    /// Transforms view matrix with another matrix.
    pub fn transform_view(&mut self, view: Mat4) {
        self.view *= view;
    }

    /// sets the zoom level of the view matrix.
    pub fn set_zoom(&mut self, zoom: f32) {
        self.eye.z = zoom * -1.0; // positive is forward, negative value is backwards.
        self.view = Mat4::look_at_rh(self.eye, self.target, self.up);
    }

    /// Returns the eye of the camera.
    pub fn get_eye(&self) -> Vec3 {
        self.eye
    }

    /// Sets the target for the camera.
    pub fn set_target(&mut self, target: Vec3) {
        self.target = target;
    }

    /// Sets the up value for the camera.
    pub fn set_up(&mut self, up: Vec3) { self.up = up; }

    /// sets xy position of the eye.
    /// Will take current z of eye as z
    pub fn set_xy_position(&mut self, x: f32, y: f32) {
        self.eye.x = x;
        self.eye.y = y;

        self.set_zoom(self.eye.z);
    }
}

/// Returns the focal length of the camera based on a field of view and screen size.
pub fn get_focal_length(fov: f32, screen_size: [u8; 2]) -> f32 {
    let aspect_ratio = (screen_size[0] as f32) / (screen_size[1] as f32);

    let film_gauge = fov.min(1.0);
    let film_height = film_gauge / aspect_ratio.max(1.0);
    let v_extent_slope = ((PI / 180.0) * 0.5 * fov).tan();

    0.5 * film_height / v_extent_slope
}

/// Returns the vertical field of view based on a height and distance.
pub fn get_vertical_fov(height: f32, distance: f32) -> f32 {
    (2.0 * (height / (2.0 * distance))).atan()
}

/// Returns the horizontal field of view based on vertical field of view and aspect ratio.
pub fn get_horizontal_fov(v_fov: f32, aspect: f32) -> f32 {
    let half_fov = (v_fov / 2.0).tan();

    2.0 * (half_fov * aspect).atan()
}

pub fn get_visible_width(h_fov: f32, distance: f32) -> f32 {
    2.0 * (h_fov / 2.0).tan() * distance
}

pub fn get_visible_height(v_fov: f32, distance: f32) -> f32 {
    2.0 * (v_fov / 2.0).tan() * distance
}

/// Get the viewable bounds of the camera
pub fn get_viewable_bounds(height: f32, distance: f32, aspect: f32) -> [f32; 2] {
    let v_fov = get_vertical_fov(height, distance);
    let h_fov = get_horizontal_fov(v_fov, aspect);
    let width = get_visible_width(h_fov, distance);
    let height = get_visible_width(v_fov, distance);

    [width, height]
}

///////

/// Rebuilds perspective values of the matrix that you pass in.
pub fn build_perspective(matrix: &mut Mat4, fov: f32, aspect: f32, near: f32, far: f32){
    *matrix = Mat4::perspective_rh(to_radians(fov), aspect, near, far);
}

pub fn translate_matrix(matrix:&mut Mat4, translateion:Vec3){
    let mut m = Mat4::from_translation(translateion);
    *matrix *= m;
}