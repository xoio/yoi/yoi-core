/// From https://github.com/spite/codevember-2021/blob/main/modules/palettes.js as well as
/// https://github.com/thi-ng/umbrella/blob/develop/packages/color-palettes/src/index.ts

pub const NATURAL: [&str; 7] = [
    "#FF6D00",
    "#FBF8EB",
    "#008B99",
    "#F8E1A6",
    "#FDA81F",
    "#B80A01",
    "#480D07",
];

pub const NATURAL2: [&str; 7] = [
    "#EF2006",
    "#350000",
    "#A11104",
    "#ED5910",
    "#F1B52E",
    "#7B5614",
    "#F7F1AC",
];

pub const SEASIDE: [&str; 7] = [
    "#FEB019",
    "#F46002",
    "#E1E7F1",
    "#0A1D69",
    "#138FE2",
    "#0652C4",
    "#D23401",
    //"#B0A12F",

];

pub const CIRCUS: [&str;7] = [
    "#F62D62",
    "#FFFFFF",
    "#FDB600",
    "#F42D2D",
    "#544C98",
    "#ECACBC",
    "#F42D2D",
];

pub const WARM: [&str; 8] = [
    "#FFFEFE",
    "#0D0211",
    "#FBCEA0",
    "#FFAD5D",
    "#530E1D",
    "#FE9232",
    "#B93810",
    "#907996",
];

pub const WARM2: [&str; 7] = [
    "#EDEBE7",
    "#13595A",
    "#DE1408",
    "#161814",
    "#E1610A",
    "#B7BDB3",
    "#9F9772",
];

/// 01T6JVh6fxawfDfX7 from thi.ng
pub const TEAL: [&str;6] = [
    "#cfcfcf",
    "#92877f",
    "#2a2621",
    "#0d4e4b",
    "#228d8a",
    "#42aca7",
];

/// 01JS99srr00rT1RGY from thi.ng
pub const BLUE_PINK: [&str;6] = [
    "#e1176f",
    "#154e66",
    "#2f749e",
    "#519fdc",
    "#95a8bb",
    "#d3dfea",
];
