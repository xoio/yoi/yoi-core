#[allow(unused_imports)]
use glam::Vec4;

#[cfg(feature = "csv")]
use csv;

/// Basic csv parser. Imported as an optional feature.
#[cfg(feature = "csv")]
pub struct CSVParser {}

#[cfg(feature = "csv")]
impl CSVParser {
    /// Parses a CSV of what is assumed to be Vec4 values. Values should be one vertex per line.
    /// It also makes the assumption that the .a/.w value will be blank and will replace with 1.0
    /// For the first element in the returned data though, that will store the number of vertices for the shape
    pub fn parse_csv(path: &str) -> Vec<Vec4> {
        let mut rdr = csv::Reader::from_path(path);
        let mut vertices = vec![];

        for result in rdr.unwrap().records() {
            let record = result.unwrap();

            let mut vert = vec![];
            for i in 0..record.len() {
                let val = record.get(i).unwrap();

                if !val.is_empty() {
                    vert.push(val.parse::<f32>().unwrap());
                }
            }

            vertices.push(Vec4::new(vert[0], vert[1], vert[2], 1.0));
        }


        // store the length of the shape in the .a/.w channel of the first vertex
        vertices[0].w = vertices.len() as f32;

        vertices
    }
}