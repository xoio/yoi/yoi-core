// adapted from https://github.com/spite/codevember-2021/blob/main/modules/gradient-linear.js

use glam::Vec4;
use yoi_math::sdf::mix;

pub struct GradientLinear {
    colors: Vec<Vec4>,
}

impl GradientLinear {
    pub fn new(colors: Vec<Vec4>) -> Self {
        GradientLinear {
            colors
        }
    }

    /// Returns a value from the gradient based on the value you pass in.
    pub fn get_at(&self, val: f32) -> Vec4 {
        let t = val.clamp(0.0, 1.0);
        let len = self.colors.len() as f32;

        let from = (t * len * 0.9999).floor();
        let to = (from + 1.0).clamp(0.0, len - 1.0);

        let fc = self.colors[from as usize];
        let ft = self.colors[to as usize];

        let p = (t - from / len) / (1.0 / len);

        let r = mix(fc.x, ft.x, p);
        let g = mix(fc.y, ft.y, p);
        let b = mix(fc.z, ft.z, p);

        Vec4::new(r, g, b, 1.)
    }

    /// Same as [get_at] but normalizes the value ahead of time to be suitable for the shader.
    pub fn get_at_normalized(&self, val: f32) -> Vec4 {
        let t = val.clamp(0.0, 1.0);
        let len = self.colors.len() as f32;

        let from = (t * len * 0.9999).floor();
        let to = (from + 1.0).clamp(0.0, len - 1.0);

        let fc = self.colors[from as usize];
        let ft = self.colors[to as usize];

        let p = (t - from / len) / (1.0 / len);

        let r = mix(fc.x, ft.x, p);
        let g = mix(fc.y, ft.y, p);
        let b = mix(fc.z, ft.z, p);

        Vec4::new(r / 255.0, g / 255.0, b / 255.0, 1.0)
    }
}