use winit::dpi::{LogicalPosition, LogicalSize};
use winit::event_loop::EventLoop;
use winit::window::{Window, WindowBuilder, WindowId};

/// The wrappings around a Wininit Window.
/// Holds general configuration information used to construct the window in the
/// app instance.
pub struct WindowData {
    pub window_size: LogicalSize<u32>,
    pub title: String,
    pub window_position: LogicalPosition<u32>,
    pub always_on_top:bool
}

/// [WindowData] implementation.
impl WindowData {
    pub fn new(width: u32, height: u32) -> Self {
        let size = LogicalSize::new(width, height);
        WindowData {
            title: String::from("Default title"),
            window_size: size,
            window_position: LogicalPosition::new(0, 0),
            always_on_top:false
        }
    }

    /// sets title for the [Window] reference. Note that [WindowData::build_window] should be called again in order
    /// to build the window with the correct title if window was pre-initialized with an [EventLoop].
    pub fn set_title(mut self, title: &str) -> Self {
        self.title = String::from(title);
        self
    }
}

impl Default for WindowData {
    fn default() -> Self {
        WindowData {
            title: String::from("Default title"),
            window_size: LogicalSize::new(1024, 768),
            window_position: LogicalPosition::new(0, 0),
            always_on_top:false
        }
    }
}