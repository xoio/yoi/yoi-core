extern crate winit;

use winit::{
    event_loop::EventLoop,
    dpi::LogicalSize,
};
use winit::event::{ElementState, ModifiersState, MouseScrollDelta};
use winit::event::DeviceEvent::Button;
use self::winit::event::{Event, WindowEvent};
use self::winit::event_loop::ControlFlow;
use self::winit::window::{WindowBuilder};

//pub type EventFn<Model> = fn(&App, &mut Model);

/*
    Basic representation of an Application. Pretty much follows how
    Nannou works and a lot of ideas are pretty much straight adaptations.
    Credit goes to those authors.
 */

pub type ModelFn<Model> = fn(&Yoi, &winit::window::Window) -> Model;
pub type DrawFn<Model> = fn(&Yoi, &mut Model);
pub type CloseFn<Model> = fn(&Yoi, &mut Model);
pub type KeyFn<Model> = fn(&Yoi, &mut Model);
pub type MouseFn<Model> = fn(&Yoi, &mut MouseState, &mut Model);

/// a simplified overview of current mouse state.
/// a simplified overview of current mouse state.
pub struct MouseState {
    pub is_pressed: bool,
    pub is_left: bool,
    pub is_right: bool,
    pub is_middle: bool,
    pub wheel_delta: Option<MouseScrollDelta>,
    pub position: Vec<f32>,
}

pub struct App<A = ()> {
    window_dimensions: LogicalSize<u32>,
    window_title: String,
    setup: Option<ModelFn<A>>,
    draw: Option<DrawFn<A>>,
    close: Option<CloseFn<A>>,
    key_down: Option<KeyFn<A>>,
    key_up: Option<KeyFn<A>>,
    mouse_down: Option<MouseFn<A>>,
    mouse_up: Option<MouseFn<A>>,
    mouse_wheel: Option<MouseFn<A>>,
    mouse_move: Option<MouseFn<A>>,
}

pub struct Yoi {
    pub window_dimensions: LogicalSize<u32>,
    pub window_title: String,
}


/// kicks everything off
#[cfg(debug_assertions)]
pub fn run_loop<A: 'static>(
    mut app: Yoi,
    setup_fn: ModelFn<A>,
    draw_fn: DrawFn<A>,
    key_down_fn: Option<KeyFn<A>>,
    key_up_fn: Option<KeyFn<A>>,
    mouse_down_fn: Option<MouseFn<A>>,
    mouse_up_fn: Option<MouseFn<A>>,
    mouse_wheel_fn: Option<MouseFn<A>>,
    mouse_move_fn: Option<MouseFn<A>>,
    close_fn: Option<CloseFn<A>>,
) {
    let mut mouse_state = MouseState {
        is_pressed: false,
        is_left: false,
        is_right: false,
        is_middle: false,
        wheel_delta: None,
        position: vec![],
    };

    // construct event loop and window.
    let e_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title(app.window_title.clone())
        .with_inner_size(winit::dpi::LogicalSize::new(app.window_dimensions.width, app.window_dimensions.height))
        .build(&e_loop).unwrap();

    // run setup loop
    let mut model = setup_fn(&app, &window);
    let mut modifiers = ModifiersState::default();


    e_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;


        // TODO should integrate some kind of object destruction when window is closed.
        match event {
            Event::WindowEvent { event, .. } => match event {
                // handles things when the window should close
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

                // handles things when the cursor moves within the window.
                WindowEvent::CursorMoved { device_id, position, modifiers: _modifiers } => {
                    if mouse_move_fn.is_some() {
                        let func = mouse_move_fn.unwrap();
                        mouse_state.position = vec![position.x as f32, position.y as f32];
                        func(&app, &mut mouse_state, &mut model);
                    }
                }

                // handles mouse wheel actions.
                WindowEvent::MouseWheel { device_id, delta, phase, modifiers } => {
                    if mouse_wheel_fn.is_some() {
                        let func = mouse_wheel_fn.unwrap();
                        mouse_state.wheel_delta = Some(delta);
                        func(&app, &mut mouse_state, &mut model);
                    }
                }

                // TODO explore what _modifiers are for exactly.
                WindowEvent::ModifiersChanged(m) => {
                    modifiers = m;
                }
                _ => (),
            },

            Event::DeviceEvent { event, device_id } => match event {
                Button { button, state } => match state {
                    ElementState::Pressed => {
                        match button {
                            1 => {
                                mouse_state.is_left = true;
                                mouse_state.is_right = false;
                                mouse_state.is_middle = false;
                                if cfg!(debug_assertations) {
                                    println!("pressed left mouse button");
                                }
                            }

                            2 => {
                                mouse_state.is_left = false;
                                mouse_state.is_right = false;
                                mouse_state.is_middle = true;

                                if cfg!(debug_assertations) {
                                    println!("Pressed middle mouse button");
                                }
                            }

                            3 => {
                                mouse_state.is_left = false;
                                mouse_state.is_right = true;
                                mouse_state.is_middle = false;

                                if cfg!(debug_assertations) {
                                    println!("pressed right mouse button");
                                }
                            }

                            _ => {}
                        }

                        mouse_state.is_pressed = true;

                        if mouse_down_fn.is_some() {
                            let func = mouse_down_fn.unwrap();
                            func(&app, &mut mouse_state, &mut model)
                        }
                    }

                    ElementState::Released => {
                        mouse_state.is_pressed = false;

                        mouse_state.is_left = false;
                        mouse_state.is_right = false;
                        mouse_state.is_middle = false;

                        if mouse_up_fn.is_some() {
                            let func = mouse_up_fn.unwrap();
                            func(&app, &mut mouse_state, &mut model)
                        }
                    }
                }
                winit::event::DeviceEvent::Key(kin) => {
                    //    println!("Device event key {:?} device id {:?}",kin,device_id);

                    // check if keydown was set and if keyboard event is pressed.
                    if key_down_fn.is_some() && kin.state == ElementState::Pressed {
                        let func = key_down_fn.unwrap();
                        func(&app, &mut model);
                    }


                    if key_up_fn.is_some() && kin.state == ElementState::Released {
                        let func = key_up_fn.unwrap();
                        func(&app, &mut model);
                    }
                }
                _ => {}
            }

            Event::MainEventsCleared => {

                // run draw loop
                draw_fn(&app, &mut model);
                window.request_redraw();
            }
            _ => (),
        }
    });
}


impl<A> App<A> where A: 'static {
    /// initializes a new app instance.
    pub fn new(title: String, width: u32, height: u32) -> Self {
        App {
            key_down: None,
            key_up: None,
            window_dimensions: LogicalSize::new(width, height),
            window_title: title,
            setup: None,
            draw: None,
            close: None,
            mouse_down: None,
            mouse_up: None,
            mouse_wheel: None,
            mouse_move: None,
        }
    }

    /// sets up the setup function
    pub fn setup(mut self, evt: ModelFn<A>) -> Self {
        self.setup = Some(evt);
        self
    }

    /// sets up the draw function
    pub fn draw(mut self, evt: DrawFn<A>) -> Self {
        self.draw = Some(evt);
        self
    }

    /// sets up the function for when a key is pressed.
    pub fn key_down(mut self, evg: KeyFn<A>) -> Self {
        self.key_down = Some(evg);
        self
    }
    pub fn key_up(mut self, evg: KeyFn<A>) -> Self {
        self.key_up = Some(evg);
        self
    }
    /// sets up the close function, this is where you define what to do when the window is closed.
    pub fn close(mut self, evt: CloseFn<A>) -> Self {
        self.close = Some(evt);
        self
    }

    /// sets the title for the window.
    pub fn set_title(mut self, title: &str) -> Self {
        self.window_title = String::from(title);
        self
    }

    /// sets callback for when the mouse is pressed.
    pub fn mouse_down(mut self, evt: MouseFn<A>) -> Self {
        self.mouse_down = Some(evt);
        self
    }

    /// sets things up when the mouse button is released
    pub fn mouse_up(mut self, evt: MouseFn<A>) -> Self {
        self.mouse_up = Some(evt);
        self
    }

    /// sets the mouse move callback
    pub fn mouse_move(mut self, evt: MouseFn<A>) -> Self {
        self.mouse_move = Some(evt);
        self
    }

    /// sets the callback when the mouse wheel is used.
    pub fn mouse_wheel(mut self, evt: MouseFn<A>) -> Self {
        self.mouse_wheel = Some(evt);
        self
    }

    /// kicks things off by calling run_loop
    pub fn run(&mut self) {
        let info = Yoi {
            window_dimensions: self.window_dimensions,
            window_title: String::from(&self.window_title),
        };


        run_loop(
            info,
            self.setup.unwrap(),
            self.draw.unwrap(),
            self.key_down,
            self.key_up,
            self.mouse_down,
            self.mouse_up,
            self.mouse_wheel,
            self.mouse_move,
            self.close);
    }
}

