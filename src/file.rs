use std::fs;

use image::DynamicImage;
use image::io::Reader as ImageReader;

pub struct FileIO {}

impl FileIO {
    /// loads a file into a string
    pub fn load_file(src: &str) -> String {
        let s = format!("Something went wrong trying to read file at {}", src);
        fs::read_to_string(src).expect(s.as_str())
    }

    /// loads an image.
    pub fn load_image(src: &str) -> DynamicImage {
        let s = format!("Unable to open image at {}", src);
        let img = ImageReader::open(src).expect(s.as_str());
        img.decode().expect("unable to decode")
    }
}

/// helper function to load files. Returns str instead of String
pub fn load_file(src: &str) -> String {
    FileIO::load_file(src)
    //FileIO::load_file_debug(src)
}