use glam::{Vec4, vec4};
use crate::gradient::GradientLinear;

/// hue to rgb converter
pub fn hue2rgb(p: f32, q: f32, _t: f32) -> f32 {
    let mut t = _t;

    if t < 0.0 {
        t += 1.0;
    }

    if t > 1.0 {
        t += 1.0;
    }

    if t < 1.0 / 6.0 {
        return p + (q - p) * 6.0 * t;
    }

    if t < 1.0 / 2.0 {
        return q;
    }

    if t < 2.0 / 3.0 {
        return p + (q - p) * 6.0 * (2.0 / 3.0 - t);
    }

    p
}

/// Normalize a RGB color that was specified from 0-255
pub fn normalize_color(val: &mut Vec4) {
    *val /= 255.0;
}

////// HEX TO RGB //////
// adapted from colorsys package
// https://github.com/emgyrz/colorsys.rs/blob/master/src/converters/hex_to_rgb.rs
const HASH: u8 = b'#';

#[derive(Debug)]
pub struct HexError {
    pub message: String,
}

/// Converts hex string to rgb
pub fn hex_to_rgb(s: &str) -> Result<[u32; 3], HexError> {
    from_hex(s.as_bytes()).map_err(|_| HexError {
        message: format!("Cannot convert")
    })
}

pub fn hex_digit_to_rgb(num: u32) -> [u32; 3] {
    let r = num >> 16;
    let g = (num >> 8) & 0x00FF;
    let b = num & 0x0000_00FF;

    [r, g, b]
}


pub fn from_hex(s: &[u8]) -> Result<[u32; 3], ()> {
    let mut buff: [u8; 6] = [0; 6];
    let mut buff_len = 0;

    for b in s {
        if !b.is_ascii() || buff_len == 6 {
            return Err(());
        }
        let bl = b.to_ascii_lowercase();
        if bl == HASH { continue; }
        if bl.is_ascii_hexdigit() {
            buff[buff_len] = bl;
            buff_len += 1;
        }
    }
    if buff_len == 3 {
        buff = [buff[0], buff[0], buff[1], buff[1], buff[2], buff[2]];
    }

    let hex_str = core::str::from_utf8(&buff).map_err(|_| ())?;
    let hex_digit = u32::from_str_radix(hex_str, 16).map_err(|_| ())?;
    Ok(hex_digit_to_rgb(hex_digit))
}


/// Builds a color palette based on a vector of colors that get passed in.
pub fn build_gradient_palette(palette: Vec<&str>) -> GradientLinear {
    //// Per-vertex colors /////
    let vert_col = palette.iter().map(|c| {
        let val = hex_to_rgb(c).expect("something went wrong");
        vec4(val[0] as f32, val[1] as f32, val[2] as f32, 1.0)
    });


    let mut col = vec![];

    for c in vert_col {
        col.push(c);
    }

    // build a gradient from the color set
    GradientLinear::new(col)
}