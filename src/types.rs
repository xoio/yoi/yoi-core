use winit::event::MouseScrollDelta;
use crate::app::{Yoi};

pub type ModelFn<Model> = fn(&Yoi, &winit::window::Window) -> Model;
pub type DrawFn<Model> = fn(&Yoi, &mut Model);
pub type CloseFn<Model> = fn(&Yoi, &mut Model);
pub type KeyFn<Model> = fn(&Yoi, &mut Model);
pub type MouseFn<Model> = fn(&Yoi, &mut MouseState, &mut Model);

/// a simplified overview of current mouse state.
pub struct MouseState {
    pub is_pressed: bool,
    pub is_left: bool,
    pub is_right: bool,
    pub is_middle: bool,
    pub wheel_delta: Option<MouseScrollDelta>,
    pub position: Vec<f32>,
}